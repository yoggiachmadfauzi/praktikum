<html>
<body>
<?php
//open daftar.php
if (isset($_POST["btndftr"])){
	$name = $_POST["txtname"];
	$nim = $_POST["txtnim"];
	$username = $_POST["txtusrnm"];
	$password= $_POST["txtpsswd"];
}else{
	die("Anda harus mengisi yang benar lewat form pendaftaran");
}
//masukan nama
if (!empty($name)){
	if (!preg_match("/^[a-zA-Z\s]*$/",$name)) {
      echo "<b>Nama hanya boleh huruf!</b><br>";
    }else{
		echo "Thanks, <b>". $name."</b><br>";
	}
}else{
	echo("Anda belum memasukkan nama <br>");
}

if (!empty($nim)){
	if (!preg_match("/^[0-9]*$/",$nim)) {
      echo "<b>NIM hanya boleh terdiri dari angka</b><br>"; 
    }else{
		echo "NIM anda : <b>". $nim."</b><br>";
	}
}else{
	echo("Anda belum memasukkan NIM <br>");
}

//masukan no hp
if (!empty($username)){
	if (!preg_match("/^[0-9a-zA-Z]*$/",$username)) {
      echo "<b>Username hanya boleh terdiri dari angka dan huruf</b><br>"; 
    }else{
		echo "Username anda : <b>". $username."</b><br>";
	}
}else{
	echo("Anda belum memasukkan Username <br>");
}

//tempat lahir
if (!empty($password)){
	if (!preg_match("/^[0-9a-zA-Z]*$/",$password)) {
      echo "<b>Password hanya boleh terdiri dari angka dan huruf</b><br>"; 
    }else{
		echo "Password anda : <b>". $password."</b><br>";
	}
}else{
	echo("Anda belum memasukkan Password <br>");
}
?>
</body>
</html>