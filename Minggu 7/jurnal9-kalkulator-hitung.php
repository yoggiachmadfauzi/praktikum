<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Modul 9</title>
        <style>
            tr td{
                padding: 5px;
            }
            .judul{
                background-color: #44ac44;
                color: white;
                font-weight: bold;
            }
            .TP{
                background-color: #eaeaea;
            }
            .J{
                background-color: #eaeaea;
            }
            .JU{
                background-color: #eaeaea;
            }
            .INDEKS{
                background-color: #eaeaea;
            }
            input[type="submit"]{
                background-color: #44ac44;
                border: none;
                color: white;
                padding: 5px;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <?php
        $TP=$_REQUEST["nilai_tp"];
        $TAW=$_REQUEST["nilai_tes_awal"];
        $J=$_REQUEST["nilai_jurnal"];
        $TAK=$_REQUEST["nilai_tes_akhir"];
        $jumlah=$TP+$TAW+$J+$TAK;
        $rata=(0.15*$TP)+(0.1*$TAW)+(0.6*$J)+(0.15*$TAK);    
        switch (true) {
            case ($rata >= 90) :
            $Grade ="A";
            break;

            case ($rata < 90 && $rata >=70) :
            $Grade ="B";
            break;

            case ($rata < 70 && $rata >=60) :
            $Grade ="C";
            break; 
            
            default:
            $Grade ="D";
            break;
        }

        ?>
        <form action="jurnal9-kalkulator-hitung.php" method="POST">
            <table>
                <tr class="judul">
                    <td colspan="2" align="center">TABEL NILAI</td>
                </tr>
                <tr class="TP">
                    <td>Nilai TP</td>
                    <td><?php  echo($TP);?></td>
                </tr>
                <tr class="TAW">
                    <td>Nilai Tes Awal</td>
                    <td><?php  echo($TAW);?></td>
                </tr>
                <tr  class="J">
                    <td>Nilai Jurnal</td>
                    <td><?php  echo($J);?></td>
                </tr>
                <tr class="TAK">
                    <td>Nilai Tes Akhir</td>
                    <td><?php  echo($TAK);?></td>
                </tr>
                <tr class="JU">
                    <td>Jumlah Nilai</td>
                    <td><?php  echo($jumlah);?></td>
                </tr>
                <tr class="R">
                    <td>Nilai Rata-rata</td>
                    <td><?php  echo($rata);?></td>
                </tr>
                <tr class="INDEKS">
                    <td>Indeks</td>
                    <td><?php  echo($Grade);?></td>
                </tr>
                <tr>
                <td>
                <p>Klik untuk  <a href="jurnal9-kalkulator.php">ini</a> Input Nilai Ulang</p>
                </td>
                </tr>
            </table>
        </form>
    </body>
</html>